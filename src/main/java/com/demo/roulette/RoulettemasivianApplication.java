package com.demo.roulette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoulettemasivianApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoulettemasivianApplication.class, args);
	}

}
