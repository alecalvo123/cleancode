package com.demo.roulette.repository;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.demo.roulette.domain.Roulette;
import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
@Repository
public class RouletteRepository implements RedisRepository {
    private static final String KEY = "Roulette";
    private static final String KEY2 = "Bet";
    private RedisTemplate<String, Roulette> redisTemplate;
    private HashOperations hashOperations;
    public RouletteRepository(RedisTemplate<String, Roulette> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
    @PostConstruct
    private void init() {
        hashOperations = redisTemplate.opsForHash();
    }
    @Override
    public Map<String, Roulette> listRoulette() {
    	
        return hashOperations.entries(KEY);
    }
    @Override
   public String findById(String id) {
    	Optional<Roulette> optional =Optional.ofNullable((Roulette) hashOperations.get(KEY, id));
    	if(optional.isPresent()) {
    		Roulette roulette = (Roulette) hashOperations.get(KEY, id);
    		roulette.setStatus("Active");
    		 hashOperations.put(KEY,id , roulette);
    		 
    		 return "rulette active";
    	}else {
    		
    		return "rulette inactive";
    	}
    }
    @Override
    public String save(Roulette roulette) {
    	String id =UUID.randomUUID().toString();
        hashOperations.put(KEY,id , roulette);
        
        return id;
    }
	@Override
	public String openRoulette(String id) {
		
		return null;
	}
	@Override
	public Bet listBet( String idroulette) {
		
		return (Bet) hashOperations.get(KEY2, idroulette);
	}
	@Override
	public String rouletteBet(Bet bet) {
		
		return null;
	}	
    
}