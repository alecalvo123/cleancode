package com.demo.roulette.repository;

import java.io.Serializable;
public class Bet implements Serializable{
private double betAmount;
private int numero;
private String color;
private String idRoulette;
private String idClient;
public String getIdClient() {
	return idClient;
}
public void setIdClient(String idClient) {
	this.idClient = idClient;
}
public String getIdRoulette() {
	return idRoulette;
}
public void setIdRoulette(String idRoulette) {
	this.idRoulette = idRoulette;
}
public double getBetAmount() {
	return betAmount;
}
public void setBetAmount(double betAmount) {
	this.betAmount = betAmount;
}
public int getNumero() {
	return numero;
}
public void setNumero(int numero) {
	this.numero = numero;
}
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}

}
