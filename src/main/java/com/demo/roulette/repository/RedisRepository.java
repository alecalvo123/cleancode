package com.demo.roulette.repository;
import java.util.Map;

import com.demo.roulette.domain.Roulette;

public interface RedisRepository {
    Map<String, Roulette> listRoulette();
    Bet listBet(String idRoulette);
    String findById(String id);
    String save(Roulette roulette);
    String rouletteBet(Bet bet);
    String openRoulette(String id);
}