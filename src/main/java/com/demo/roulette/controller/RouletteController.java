package com.demo.roulette.controller;
import org.springframework.web.bind.annotation.*;
import com.demo.roulette.domain.Roulette;
import com.demo.roulette.repository.Bet;
import com.demo.roulette.repository.RouletteRepository;
import java.util.Map;

@RestController
public class RouletteController {
    private RouletteRepository rouletteRepository;
    public RouletteController(RouletteRepository rouletteRepository) {
        this.rouletteRepository = rouletteRepository;
    }
    @GetMapping("/roulette")
    public Map<String, Roulette> findAll() {
        return rouletteRepository.listRoulette();
    }
    @RequestMapping("/roulette/{id}")
    public String findById(@PathVariable String id) {
    		
       return   rouletteRepository.findById(id);
    }

    @PostMapping("/roulette")
    public String createRoulette(@RequestBody Roulette roulette) {
        return rouletteRepository.save(roulette);
    }   
    @PostMapping("/bet/{idClient}")
    public String createBet(@RequestBody Bet bet, @PathVariable String idClient) {
        if(bet.getBetAmount() <= 0 || bet.getBetAmount()> 10000 || bet.getNumero()<0 || bet.getNumero()>36 || idClient.isEmpty() ) {
        	
        	return "check your bet";
        }else {
        	bet.setColor(idClient);
        	rouletteRepository.rouletteBet(bet);
       	
        	return "sucessfull";
        	}
    }
   @GetMapping("/bet/{idRoulette}")
   public Bet lisBet(@PathVariable String id) {
		
       return (Bet) rouletteRepository.listBet(id);
    }
}